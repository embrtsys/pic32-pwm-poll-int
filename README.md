# README #

This project contains a demo of generating a PWM signal by software. The software based signal generation allows to generate two complementary signals to drive a motor H-bridge. 


### Contents ###

The program consists of a single main file. In the repository, you will find 4 tagged versions: 

Version | Description
--------|----------------------------------------------------------------------
[v1](https://bitbucket.org/eletronica4/pic32-pwm-poll-int/commits/tag/v1)      | Periodical event, polling based. 
[v2](https://bitbucket.org/eletronica4/pic32-pwm-poll-int/commits/tag/v2)      | Periodical event, interrupt based
[v3](https://bitbucket.org/eletronica4/pic32-pwm-poll-int/commits/tag/v3)      | PWM signal, with one event at each rising and falling edge, polling. 
[v4](https://bitbucket.org/eletronica4/pic32-pwm-poll-int/commits/tag/v4)      | PWM signal, with one event at each rising and falling edge, interrupt.