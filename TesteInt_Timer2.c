/* 
 * File:   TesteInt_Timer2.c
 * Author: pf
 *
 * Generation of a programmable PWM output by software, to enable the 
 * generation of two complementary signals, if necessary. 
 * 
 * The PWM period is defined by the timer 2 Period Register, PR2; the ON
 * interval is defined by Output Compare 2 Compare Register, OC2R. 
 * 
 * The main() switches T_on between two values. 
 * 
 * Version by polling (no interrupts)
 * 
 * Created on 04 April 2017, 22:12
 */

#include <stdio.h>
#include <stdlib.h>
#include <xc.h>
#include <sys/attribs.h>

#define _SUPPRESS_PLIB_WARNING 1
#include <plib.h>

#define SYSCLK  80000000L   // System clock frequency, in Hz
#define PBUSCLK 40000000L   // Peripheral bus clock

/*
 * 
 */
int main(int argc, char** argv) {

    uint32_t dummy=0;
    uint8_t state=0;
    
    // Performance optimization (flash access time, enable instruct and data cache,... and PBClock setup
    SYSTEMConfigPerformance(SYSCLK);
    mOSCSetPBDIV(OSC_PB_DIV_2); // This is necessary since SYSTEMConfigPerformance defaults FPBDIV to DIV_1

    INTEnableSystemMultiVectoredInt();

    TRISDbits.TRISD0 = 0;

    /* Program Timer 2 */
    T2CON = 0;              /* Clear all registers */
    T2CONbits.ON = 0;       /* Stop the timer */
    T2CONbits.TCKPS = 0x07; /* Set prescaler to 256 */
    TMR2 = 0;               /* Clear the timer register */
    PR2 = 0xFF00;           /* Set the period register */
 
    /* Program Output Compare 2 */
    OC2CONbits.ON = 0;      /* Disable comparator (for the moment...) */
    OC2CONbits.SIDL = 0;    /* Continue on Idle */
    OC2CONbits.OC32 = 0;    /* Compare to a 16bit timer */
    OC2CONbits.OCTSEL = 0;  /* Compare with Timer 2 */
    OC2CONbits.OCM = 3;     /* Single Compare Mode, Toggle */
 
    OC2R = 0x2000;          /* Set Output Compare 2 Compare Register */
    OC2CONbits.ON = 1;      /* Enable OC2 */
    
    /* Timer 2 */
    IFS0bits.T2IF = 0;      /* Clear Interrupt Flag */
    
    /* Output Compare 1 */
    IFS0bits.OC2IF = 0;     /* Clear Interrupt Flag */
    

    IPC2bits.T2IP = 2;      /* Timer 2 priority: 2 */
    IPC2bits.T2IS = 0;      /* Timer 2 sub-priority: 0 */
    
    IPC2bits.OC2IP = 2;     /* Output Compare 2 priority: 2 */
    IPC2bits.OC2IS = 0;     /* Output Compare 2 sub-priority: 0 */

    IEC0bits.T2IE = 1;      /* Set Timer 2 Interrupt Enable bit */
    IEC0bits.OC2IE = 1;     /* Set Output Compare 2 Interrupt Enable bit */
    
    T2CONbits.ON = 1;       /* Start the timer */
    
    while (1) {
        dummy++;
        if(dummy == 12000000L){
            
            dummy = 0;
            
            /* Change T_on for a new value */
            if(state == 0){
                state = 1;
                OC2R = 0xC000;
            }
            else{
                state = 0;
                OC2R = 0x2000;
            }
        }
        
    }

    return (EXIT_SUCCESS);
}

/*
 * Service routine for Timer 2 Interrupt
 */
void __ISR(_TIMER_2_VECTOR, IPL2AUTO) Timer2ISRHandler(void){
    
    /* Timer 2 reached end of count */
    IFS0bits.T2IF = 0;          /* Clear Timer 2 Interrupt Flag */
    LATDbits.LATD0 = 1;
}

/*
 * Service routine for Output Compare 2 Interrupt
 */
void __ISR(_OUTPUT_COMPARE_2_VECTOR, IPL2AUTO) OutputCompare2ISRHandler(void){

    /* Output compare match */
    IFS0bits.OC2IF = 0; /* Clear Interrupt flag */
    LATDbits.LATD0 = 0;    
}